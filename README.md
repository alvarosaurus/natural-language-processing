This project provides a Dockerfile for building an environment suitable for natural language processing,
based on Python 3 and the NLTK library (http://nltk.org).

You can get a ready to use Docker Image from: https://cloud.docker.com/u/aot29/repository/docker/aot29/nlp

Example data set: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2359073.svg)](https://doi.org/10.5281/zenodo.2359073)

# Building from source

## Installing Docker
Install Docker for your operating system (e.g. on Debian https://docs.docker.com/install/linux/docker-ce/debian/)

If on Linux, add your user to the "docker" group for convenience
```
sudo usermod -aG docker your-user-name
```
Restart the machine.

## Building a Docker image
This will download all the required libraries and pack them in a Docker image.
The completed image will be called "nltk" and will occupy 4.39GB of disk space.
As the library is relatively large, building the image can take a while.
```
make all
```

## Testing and running
To run the NLTK container, call
```
make run
```
To test the container, call
```
make test
```
This can take a while and requires 4.39GB of disk space.
```
cd natural-language-processing
make
make run
make test
```
The test will throw some warnings that you can safely ignore, as long as all the tests pass.

# Using the container
You can connect to the running Docker container and open a bash prompt by calling:
```
docker exec -ti nltk bash
```
See the ressources section for some pointers to what you can do with NLTK.
Calling ```exit``` will disconnect from the running Docker container.

Alternatively, you can run a Python prompt:
Start the container as shown above. You can connect to the container and open a Python prompt directly by calling:
```
docker run -ti nltk python
```
```Ctrl-d``` will close the Python prompt and disconnect from the container.

## Stopping 
To stop the container without deleting the image, call
```
make stop
```

## Uninstalling
To delete the image, call
```
make remove
```

You can then uninstall Docker if necessary.

# Ressources
Beysolow II, Taweh (2018). Applied natural language processing with Python: implementing machine learning and deep learning algorithms for natural language processing
. New York, Apress. ISBN 978-1-4842-3732-8

Bird, S., Klein, E., Loper, E. (2018). Natural Language Processing with Python, https://www.nltk.org/book/

NLTK - Python programming tutorial, https://pythonprogramming.net/tokenizing-words-sentences-nltk-tutorial/

NLTK 3.3 documentation, http://www.nltk.org/

