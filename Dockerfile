FROM python:3

WORKDIR /usr/src/app

RUN apt-get update \
    && apt-get -y install nano vim less \
    g++ libpcre3 libpcre3-dev swig \
    pulseaudio libpulse-dev \
    poppler-utils tesseract-ocr \
    python-pdfminer

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

RUN echo Downloading 3.3 GB, this could take a while...
RUN export PYTHONUNBUFFERED=0
RUN python -c 'import nltk; nltk.download("all");'
