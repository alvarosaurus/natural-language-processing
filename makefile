export $(shell sed 's/=.*//' config.ini)

# Name of the container with NLTK
NLTK_CONTAINER=nlp

###########################################
#
# Build the image.
#
###########################################
all:
	docker build -f Dockerfile -t ${NLTK_CONTAINER} .
	docker images 


#############################################
#
# Start the container.
#
#############################################
run:stop
	docker run \
		--restart no \
		--name ${NLTK_CONTAINER} \
		-v $(shell pwd)/unittests:/unittests \
		-td \
		${NLTK_CONTAINER}
	docker ps


#############################################
#
# Test the container.
#
#############################################
test:
	docker exec -ti ${NLTK_CONTAINER} script -q -c "cd / && python -Bm unittest discover -s unittests -p '*.py'"


#############################################
#
# Stop and remove the container.
#
#############################################
stop:
	-docker stop ${NLTK_CONTAINER}
	-docker rm ${NLTK_CONTAINER}


#############################################
#
# Remove the image and container.
#
#############################################
remove:
	-docker stop ${NLTK_CONTAINER}
	-docker rm ${NLTK_CONTAINER}
	-docker rmi ${NLTK_CONTAINER}

