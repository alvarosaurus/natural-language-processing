"""Test NLTK."""
from nltk.tokenize import sent_tokenize
from nltk.corpus import gutenberg
from nltk.corpus import wordnet
from NLTKBaseTestCase import NLTKBaseTestCase


class CorporaLexicaTestCase(NLTKBaseTestCase):

    def test01(self):
        """Test that a text can be loaded from the corpora."""
        # sample text
        sample = gutenberg.raw("bible-kjv.txt")
        tok = sent_tokenize(sample)

        self.assertTrue("In the beginning God created the heaven and the earth" in tok[0])

    def test02(self):
        """Test that synonyms and antonyms can be found."""
        synonyms = []
        antonyms = []

        synset = wordnet.synsets("good")

        for syn in synset:
            for l in syn.lemmas():
                synonyms.append(l.name())
                if l.antonyms():
                    antonyms.append(l.antonyms()[0].name())

        self.assertTrue("well" in synonyms)
        self.assertTrue("bad" in antonyms)

    def test03(self):
        """Test that similarity between words can be computed."""
        # Wu and Palmer method
        # https://metacpan.org/pod/release/TPEDERSE/WordNet-Similarity-1.03/lib/WordNet/Similarity/wup.pm
        w1 = wordnet.synset('ship.n.01')
        w2 = wordnet.synset('boat.n.01')
        similarity = w1.wup_similarity(w2)
        self.assertTrue(similarity > 0.9)
