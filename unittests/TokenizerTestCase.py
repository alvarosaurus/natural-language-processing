"""Test NLTK."""
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
from NLTKBaseTestCase import NLTKBaseTestCase


class TokenizerTestCase(NLTKBaseTestCase):
    """Test that the sentence and word tokenizers works."""

    EXAMPLE_TEXT = "Hello Mr. Smith, how are you doing today? \
    The weather is great, and Python is awesome. \
    The sky is pinkish-blue. You shouldn't eat cardboard."

    def test01(self):
        """Test that the sentence tokenizer works."""
        tk = sent_tokenize(TokenizerTestCase.EXAMPLE_TEXT)
        self.assertEqual(4, len(tk))
        self.assertTrue("You shouldn't eat cardboard.", tk[3])

    def test02(self):
        """Test that the word tokenizer works."""
        tk = word_tokenize(TokenizerTestCase.EXAMPLE_TEXT)
        self.assertEqual(31, len(tk))
        self.assertTrue("pinkish-blue" in tk)

    def test03(self):
        """Test that stop words can be filtered in English."""

        stop_words = set(stopwords.words('english'))
        tk = word_tokenize(TokenizerTestCase.EXAMPLE_TEXT)
        self.assertEqual(31, len(tk))

        filtered_tk = [w for w in tk if not w in stop_words]
        self.assertEqual(22, len(filtered_tk))
