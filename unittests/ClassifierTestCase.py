"""Test NLTK."""
import nltk
import random
from nltk.corpus import movie_reviews
from NLTKBaseTestCase import NLTKBaseTestCase


class ClassifierTestCase(NLTKBaseTestCase):
    """Test classifiers built into NLTK."""

    def setUp(self):
        self.words = movie_reviews.words()
        """Load movie reviews for testing."""
        self.testDocs = [(list(movie_reviews.words(fileid)), category)
                         for category in movie_reviews.categories()
                         for fileid in movie_reviews.fileids(category)]
        """movie reviews by category."""
        random.shuffle(self.testDocs)

    def test01(self):
        """Compute the frequency distribution of words."""
        commonWords = self._freqDist(self.words).most_common(15)  # list of tuples
        keys = [item[0] for item in commonWords]

        self.assertTrue("the" in keys)
        self.assertTrue("a" in keys)
        self.assertTrue("and" in keys)

    def test02(self):
        """Compute the frequency of a given word."""
        allWords = self._freqDist(self.words)
        self.assertEqual(566, allWords["hollywood"])

    def test03(self):
        """Test that words are present or absent in given text."""
        commonWords = self._commonWords(self.words)
        features = self._findFeatures(movie_reviews.words('neg/cv000_29416.txt'), commonWords)
        self.assertTrue(features["the"])
        self.assertTrue(features["a"])
        self.assertTrue(features["and"])
        self.assertFalse(features["negative"])
        self.assertFalse(features["attraction"])
        self.assertFalse(features["wing"])

    def test04(self):
        commonWords = self._commonWords(self.words)
        featuresets = [(self._findFeatures(rev, commonWords), category) for (rev, category) in self.testDocs]
        """Dict of features and category ('neg' or 'pos')."""

