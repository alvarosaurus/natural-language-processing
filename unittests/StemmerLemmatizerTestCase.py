"""Test NLTK."""
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from NLTKBaseTestCase import NLTKBaseTestCase


class StemmerLenmmatizerTestCase(NLTKBaseTestCase):
    """Test that the stemmer and lemmatizer work."""
    EXAMPLE_TEXT = "It is important to by very pythonly \
    while you are pythoning with python. All pythoners have \
    pythoned poorly at least once."

    def test01(self):
        """Test that the English stemmer works."""
        ps = PorterStemmer()
        tk = word_tokenize(StemmerLenmmatizerTestCase.EXAMPLE_TEXT)
        stem = [ps.stem(w) for w in tk]
        pythons = []
        for w in stem:
            if w == 'python':
                pythons.append(w)
        self.assertEqual(4, len(pythons))

    def test02(self):
        """Test that the English lemmatizer works."""
        lemmatizer = WordNetLemmatizer()

        self.assertEqual("cat", lemmatizer.lemmatize("cats"))
        self.assertEqual("cactus", lemmatizer.lemmatize("cacti"))
        self.assertEqual("goose", lemmatizer.lemmatize("geese"))
        self.assertEqual("rock", lemmatizer.lemmatize("rocks"))
        self.assertEqual("python", lemmatizer.lemmatize("python"))
        self.assertEqual("good", lemmatizer.lemmatize("better", pos="a"))
        self.assertEqual("best", lemmatizer.lemmatize("best", pos="a"))
        self.assertEqual("run", lemmatizer.lemmatize("run"))
        self.assertEqual("run", lemmatizer.lemmatize("run", 'v'))
