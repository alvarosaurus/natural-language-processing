"""A base class for unit tests using NLTK."""
import nltk
import warnings
import unittest


class NLTKBaseTestCase(unittest.TestCase):
    """A base class for unit tests using NLTK."""

    def setUp(self):
        # supress  warnings
        warnings.filterwarnings("ignore")

    def _commonWords(self, words):
        """Most common words"""
        common = dict(self._freqDist(words).most_common(3000))
        commonWords = common.keys()
        return commonWords

    def _freqDist(self, words):
        """Compute the frequency distribution of words."""
        allWords = []
        for w in words:
            allWords.append(w.lower())

        allWords = nltk.FreqDist(allWords)
        return allWords

    def _findFeatures(self, document, commonWords):
        """
        Mark presence or absence of each common word in given text.

        @return: list of tuples (word:True/False)
        """
        words = set(document)
        features = {}
        for w in commonWords:
            features[w] = (w in words)

        return features
