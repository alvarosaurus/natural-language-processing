"""Test NLTK."""
import nltk
import random
from nltk.corpus import movie_reviews

from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression,SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from NLTKBaseTestCase import NLTKBaseTestCase


class SklearnTestCase(NLTKBaseTestCase):
    """Test classifiers built into sklearn."""

    def setUp(self):
        self.words = movie_reviews.words()
        self.testDocs = [(list(movie_reviews.words(fileid)), category)
                         for category in movie_reviews.categories()
                         for fileid in movie_reviews.fileids(category)]
        """Load movie reviews for testing."""
        random.shuffle(self.testDocs)
        commonWords = self._commonWords(self.words)
        self.featuresets = [(self._findFeatures(rev, commonWords), category) for (rev, category) in self.testDocs]
        """Dict of features and category ('neg' or 'pos')."""
        # Split into training and testing sets
        self.training_set = self.featuresets[:1900]
        self.testing_set = self.featuresets[1900:]

    def test01(self):
        """Test Bayes classifier."""
        classifier = nltk.NaiveBayesClassifier.train(self.training_set)
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)

    def test02(self):
        """Test MultinomialNB classifier."""
        classifier = SklearnClassifier(MultinomialNB())
        classifier.train(self.training_set)
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)

    def test03(self):
        """Test BernoulliNB classifier."""
        classifier = SklearnClassifier(BernoulliNB())
        classifier.train(self.training_set)
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)

    def test04(self):
        """Test LogisticRegression classifier."""
        classifier = SklearnClassifier(LogisticRegression(solver='lbfgs', max_iter=1000))
        classifier.train(self.training_set)
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)

    def test05(self):
        """Test SGD classifier."""
        classifier = SklearnClassifier(SGDClassifier(max_iter=5, tol=None))
        classifier.train(self.training_set)
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)

    def test06(self):
        """Test SVC classifier."""
        classifier = SklearnClassifier(SVC(gamma='auto'))
        classifier.train(self.training_set)
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)

    def test07(self):
        """Test LinearSVC classifier."""
        classifier = SklearnClassifier(LinearSVC())
        classifier.train(self.training_set)
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)

    def test08(self):
        """Test NuSVC classifier."""
        classifier = SklearnClassifier(NuSVC(gamma='auto'))
        classifier.train(self.training_set)
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)
