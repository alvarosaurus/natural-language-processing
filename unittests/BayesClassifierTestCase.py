"""Test NLTK."""
import nltk
import random
import pickle
import os
from nltk.corpus import movie_reviews
from NLTKBaseTestCase import NLTKBaseTestCase


class BayesClassifierTestCase(NLTKBaseTestCase):
    """Test classifiers built into NLTK."""

    def setUp(self):
        self.words = movie_reviews.words()
        self.testDocs = [(list(movie_reviews.words(fileid)), category)
                         for category in movie_reviews.categories()
                         for fileid in movie_reviews.fileids(category)]
        """Load movie reviews for testing."""
        random.shuffle(self.testDocs)
        commonWords = self._commonWords(self.words)
        self.featuresets = [(self._findFeatures(rev, commonWords), category) for (rev, category) in self.testDocs]
        """Dict of features and category ('neg' or 'pos')."""
        # Split into training and testing sets
        self.training_set = self.featuresets[:1900]
        self.testing_set = self.featuresets[1900:]

    def test01(self):
        """Test naive bayes classifier."""

        # instantiate and train the classifier
        classifier = nltk.NaiveBayesClassifier.train(self.training_set)
        # test the classifier
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)
        self.assertTrue(classifier.most_informative_features(1)[0] == ('outstanding', True))

    def test02(self):
        """Test saving and loading a trained classifier."""
        # instantiate and train the classifier
        classifier = nltk.NaiveBayesClassifier.train(self.training_set)
        # save the classifier
        classifier_file = "naivebayes.pickle"
        save_classifier = open(classifier_file, "wb")
        pickle.dump(classifier, save_classifier)
        save_classifier.close()

        # load the classifier
        classifier_f = open(classifier_file, "rb")
        classifier = pickle.load(classifier_f)
        classifier_f.close()

        # delete the saved classifier
        os.remove(classifier_file)

        # test the classifier
        self.assertTrue(nltk.classify.accuracy(classifier, self.testing_set) > 0.5)
        self.assertTrue(('outstanding', True) in classifier.most_informative_features(1))
