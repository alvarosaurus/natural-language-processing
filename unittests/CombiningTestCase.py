"""Test NLTK."""
import nltk
from nltk.classify import ClassifierI
import random
from nltk.corpus import movie_reviews
import pickle
from statistics import mode

from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC

from NLTKBaseTestCase import NLTKBaseTestCase


class CombiningTestCase(NLTKBaseTestCase):
    """Test combining classification algorithms."""

    def setUp(self):
        self.words = movie_reviews.words()
        self.testDocs = [(list(movie_reviews.words(fileid)), category)
                         for category in movie_reviews.categories()
                         for fileid in movie_reviews.fileids(category)]
        """Load movie reviews for testing."""
        random.shuffle(self.testDocs)
        commonWords = self._commonWords(self.words)
        self.featuresets = [(self._findFeatures(rev, commonWords), category) for (rev, category) in self.testDocs]
        """Dict of features and category ('neg' or 'pos')."""
        # Split into training and testing sets
        self.training_set = self.featuresets[:1900]
        self.testing_set = self.featuresets[1900:]

        # make a classifier that combines all classifiers
        self._makeClassifiers()
        self.votedClassifier = VoteClassifier(
            self.bayesClassifier,
            self.NuSVCClassifier,
            self.LinearSVCClassifier,
            self.SGDClassifier,
            self.NBClassifier,
            self.BernoulliClassifier,
            self.LogisticClassifier)

    def test01(self):
        """Test voted classifier"""
        self.assertTrue(nltk.classify.accuracy(self.votedClassifier, self.testing_set) > 0.5)

    def _makeClassifiers(self):
        """Bayes classifier."""
        self.bayesClassifier = nltk.NaiveBayesClassifier.train(self.training_set)
        """MultinomialNB classifier."""
        self.NBClassifier = SklearnClassifier(MultinomialNB()).train(self.training_set)
        """BernoulliNB classifier."""
        self.BernoulliClassifier = SklearnClassifier(BernoulliNB()).train(self.training_set)
        """LogisticRegression classifier."""
        self.LogisticClassifier = SklearnClassifier(LogisticRegression(solver='lbfgs', max_iter=1000)).train(self.training_set)
        """SGD classifier."""
        self.SGDClassifier = SklearnClassifier(SGDClassifier(max_iter=5, tol=None)).train(self.training_set)
        """SVC classifier."""
        self.SVCClassifier = SklearnClassifier(SVC(gamma='auto')).train(self.training_set)
        """LinearSVC classifier."""
        self.LinearSVCClassifier = SklearnClassifier(LinearSVC()).train(self.training_set)
        """NuSVC classifier."""
        self.NuSVCClassifier = SklearnClassifier(NuSVC(gamma='auto')).train(self.training_set)


class VoteClassifier(ClassifierI):
    """
    Custom classifier.

    @see: https://pythonprogramming.net/combine-classifier-algorithms-nltk-tutorial/
    """
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)
        return mode(votes)
