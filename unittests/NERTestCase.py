"""Test NLTK."""
import nltk
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer
from NLTKBaseTestCase import NLTKBaseTestCase


class NERTestCase(NLTKBaseTestCase):
    """Test that part of speech tagging, named entity recognition, chunking and chinking work."""

    def setUp(self):
        """Load 2 texts, one for training the other for testing."""
        self.train_text = state_union.raw("2005-GWBush.txt")
        self.sample_text = state_union.raw("2006-GWBush.txt")

    def test01(self):
        """Test tagging."""        
        custom_sent_tokenizer = PunktSentenceTokenizer(self.train_text)
        tokenized = custom_sent_tokenizer.tokenize(self.sample_text)
        custom_sent_tokenizer = PunktSentenceTokenizer(self.train_text)
        tokenized = custom_sent_tokenizer.tokenize(self.sample_text)
        maxsent = 5

        tagged = None
        try:
            for i in tokenized[:maxsent]:
                words = nltk.word_tokenize(i)
                tagged = nltk.pos_tag(words)

        except Exception as e:
            print(str(e))

        # at least some stuff is tagged correctly
        # proper noun
        self.assertTrue(('George', 'NNP') in tagged)
        # preposition/subordinating conjunction
        self.assertTrue(('of', 'IN') in tagged)
        # verb
        self.assertTrue(('reacts', 'VBZ') in tagged)
        # determinant
        self.assertTrue(('the', 'DT') in tagged)

    def test02(self):
        """Test named entity recognition."""

        custom_sent_tokenizer = PunktSentenceTokenizer(self.train_text)
        tokenized = custom_sent_tokenizer.tokenize(self.sample_text)

        subtrees = []
        try:
            for i in tokenized[5:]:
                words = nltk.word_tokenize(i)
                tagged = nltk.pos_tag(words)
                namedEnt = nltk.ne_chunk(tagged, binary=False)
                for subtree in namedEnt.subtrees():
                    subtrees.append("[%s %s]" % (subtree.label(), str(subtree.leaves())))
        except Exception as e:
            print(str(e))

        self.assertTrue("[ORGANIZATION [('Supreme', 'NNP'), ('Court', 'NNP')]]" in subtrees)
        self.assertTrue("[PERSON [('John', 'NNP'), ('Roberts', 'NNP')]]" in subtrees)
        self.assertTrue("[GPE [('United', 'NNP'), ('States', 'NNPS')]]" in subtrees)

    def test03(self):
        """Test chunking."""
        custom_sent_tokenizer = PunktSentenceTokenizer(self.train_text)
        tokenized = custom_sent_tokenizer.tokenize(self.sample_text)

        subtrees = []
        try:
            chunkGram = r"""Chunk: {<RB.?>*<VB.?>*<NNP>+<NN>?}"""
            chunkParser = nltk.RegexpParser(chunkGram)
            for i in tokenized:
                words = nltk.word_tokenize(i)
                tagged = nltk.pos_tag(words)
                chunked = chunkParser.parse(tagged)
                for subtree in chunked.subtrees(filter=lambda t: t.label() == 'Chunk'):
                    subtrees.append(str(subtree.leaves()))
        except Exception as e:
            print(str(e))

        # at least some stuff is chunked correctly
        self.assertTrue("[('Staff', 'NNP'), ('Sergeant', 'NNP'), ('Dan', 'NNP'), ('Clay', 'NNP')]" in subtrees)
        self.assertTrue("[('American', 'NNP'), ('taxpayer', 'NN')]" in subtrees)
        self.assertTrue("[('Martin', 'NNP'), ('Luther', 'NNP'), ('King', 'NNP')]" in subtrees)

    def test04(self):
        """Test chinking."""
        custom_sent_tokenizer = PunktSentenceTokenizer(self.train_text)
        tokenized = custom_sent_tokenizer.tokenize(self.sample_text)

        subtrees = []
        try:
            chunkGram = r"""Chunk: {<.*>+}
                                    }<VB.?|IN|DT|TO>+{"""
            chunkParser = nltk.RegexpParser(chunkGram)
            for i in tokenized:
                words = nltk.word_tokenize(i)
                tagged = nltk.pos_tag(words)
                chunked = chunkParser.parse(tagged)
                for subtree in chunked.subtrees(filter=lambda t: t.label() == 'Chunk'):
                    subtrees.append(str(subtree.leaves()))
        except Exception as e:
            print(str(e))

        # at least some stuff is chinked correctly
        self.assertTrue("[('European', 'NNP'), ('Union', 'NNP')]" in subtrees)
        self.assertTrue("[('American', 'JJ'), ('people', 'NNS')]" in subtrees)
        self.assertTrue("[('Middle', 'NNP'), ('Eastern', 'NNP'), ('oil', 'NN')]" in subtrees)
