"""Test NLTK."""
import nltk
import random
from nltk.corpus import movie_reviews
from NLTKBaseTestCase import NLTKBaseTestCase


class FeaturesetTestCase(NLTKBaseTestCase):
    """Test that featuresets can be created."""

    def setUp(self):
        super().setUp()
        # load all movie reviews from corpus
        all_documents = [
            (list(movie_reviews.words(fileid)), category)
            for category in movie_reviews.categories()
            for fileid in movie_reviews.fileids(category)
        ]
        # get 100 reviews a ramdom for testing
        random.shuffle(all_documents)
        self.documents = all_documents[:100]

    def test01(self):
        """Find the 100 most common words."""
        all_words = []

        for w in movie_reviews.words():
            all_words.append(w.lower())
        all_words = nltk.FreqDist(all_words)
        common = dict(nltk.FreqDist(all_words).most_common(100))
        commonWords = list(common.keys())
        self.assertTrue('.' in commonWords)
        self.assertTrue('the' in commonWords)
        self.assertTrue('a' in commonWords)

    def test02(self):
        """Find the featured words in a text."""
        all_words = []

        for w in movie_reviews.words():
            all_words.append(w.lower())
        all_words = nltk.FreqDist(all_words)
        common = dict(nltk.FreqDist(all_words).most_common(100))
        commonWords = list(common.keys())
        features = (
            self._find_features(
                movie_reviews.words('neg/cv000_29416.txt'),
                commonWords
            )
        )
        self.assertTrue(features['.'])
        self.assertTrue(features['the'])
        self.assertFalse(features['at'])

    def test03(self):
        """Find the featuresets of all documents."""
        all_words = []

        for w in movie_reviews.words():
            all_words.append(w.lower())
        all_words = nltk.FreqDist(all_words)
        common = dict(nltk.FreqDist(all_words).most_common(100))
        commonWords = list(common.keys())
        featuresets = [
            (
                self._find_features(rev, commonWords),
                category
            )
            for (rev, category) in self.documents
        ]
        self.assertEquals(100, len(featuresets))

    def _find_features(self, document, word_features):
        words = set(document)
        features = {}
        for w in word_features:
            features[w] = (w in words)

        return features
